<?php
/**
 * @file
 * Contains facebook_status_og_og_context.
 */

module_load_include('inc', 'facebook_status', 'includes/utility/facebook_status.contexts');

/**
 * Provides an enhanced OG context.
 */
class facebook_status_og_og_context extends facebook_status_og_context {
  /**
   * Returns an object representing the recipient, determined automatically.
   */
  function find_recipient() {
    $group = menu_get_object();
    if (empty($group)) {
      if (is_numeric(arg(1))) {
        $group = node_load(arg(1));
      }
      else {
        $group = og_get_group_context();
      }
    }

    return $this->is_applicable() ? $group : new stdClass();
  }

  /**
   * Determines whether the sender has permission to post a status message to
   * the recipient's stream.
   */
  function access_add($recipient, $sender) {
    return (
      og_user_access($recipient->nid, 'post status to group', $sender)
      || og_user_access($recipient->nid, 'post status as group', $sender)
    );
  }

  /**
   * Determines whether $account has permission to edit $status.
   */
  function access_edit($status, $account) {
    return parent::access_edit($status, $account) || $status->sender == $account->uid || ($status->gid == $status->recipient && og_user_access($status->recipient, 'post status as group', $account));
  }

  /**
   * Determines whether an account has permission to view a status.
   */
  function access_view($status, $account) {
    return (og_user_access($status->recipient, 'view statuses posted to group', $account));
  }
}
