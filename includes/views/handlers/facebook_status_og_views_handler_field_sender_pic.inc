<?php
/**
 * @file
 * Contains facebook_status_og_views_handler_field_sender_pic.
 */

/**
 * Shows the profile picture for the user or group that sent the status.
 */
class facebook_status_og_views_handler_field_sender_pic extends facebook_status_views_handler_field_sender_pic {
  /**
   * Construct a new field handler.
   */
  function construct() {
    parent::construct();

    $this->additional_fields['gid'] = 'gid';
  }

  /**
   * Render the field.
   */
  function render($values) {
    // If this status does not have a gid set then it was not posted as a group.
    // Therefore, we can fall through to the default.
    $gid = $this->get_value($values, 'gid');
    if (empty($gid)) {
      return parent::render($values);
    }

    // If an imagecache preset was selected in the options, set it on the group
    // object so that it can be properly read later.
    $group = node_load($gid);
    if (isset($this->options['imagecache_preset']) && $this->options['imagecache_preset']) {
      $group->imagecache_preset = $this->options['imagecache_preset'];
    }

    // If we were able to build an image for the group then render it as the
    // field.
    $output = '';
    $image = facebook_status_og_display_group_image($group);
    if (!empty($image)) {
      $output = '<div class="facebook-status-sender-picture user-picture">' . $image . '</div>';
    }

    return $output;
  }
}
