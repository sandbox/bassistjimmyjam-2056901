<?php
/**
 * @file
 * Contains facebook_status_og_views_handler_field_sender_name.
 */

/**
 * Field handler to display the name of the sender.
 *
 * If the status was posted as a group, then the name will be that of the group.
 */
class facebook_status_og_views_handler_field_sender_name extends views_handler_field {
  /**
   * Construct a new field handler.
   */
  function construct() {
    parent::construct();

    $this->additional_fields['gid'] = 'gid';
  }

  /**
   * Information about options for all kinds of purposes will be held here.
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['link_to_sender'] = array('default' => TRUE);
    $options['overwrite_anonymous'] = array('default' => FALSE);
    $options['anonymous_text'] = array('default' => '', 'translatable' => TRUE);

    return $options;
  }

  /**
   * Options form.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['link_to_sender'] = array(
      '#type' => 'checkbox',
      '#title' => t('Link this field to the sender'),
      '#description' => t('This will override any other link you have set.'),
      '#default_value' => !empty($this->options['link_to_sender']),
    );
    $form['overwrite_anonymous'] = array(
      '#title' => t('Overwrite the value to display for anonymous users'),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['overwrite_anonymous']),
      '#description' => t('If selected, you will see a field to enter the text to use for anonymous users.'),
    );
    $form['anonymous_text'] = array(
      '#title' => t('Text to display for anonymous users'),
      '#type' => 'textfield',
      '#default_value' => $this->options['anonymous_text'],
      '#process' => array('views_process_dependency'),
      '#dependency' => array(
        'edit-options-overwrite-anonymous' => array(1),
      ),
    );
  }

  /**
   * Render the field.
   */
  function render($values) {
    // If this status does not have a gid set then it was not posted as a group.
    // Therefore, we can fall through to the default.
    $output = '';
    $gid = $this->get_value($values, 'gid');
    if (empty($gid)) {
      $uid = $this->get_value($values);
      $account = user_load($uid);

      // If this is the anonymous user and we have have overwritten text for the
      // user name then set it on the account.
      if ($account->uid == 0 && !empty($this->options['overwrite_anonymous'])) {
        $account->name = check_plain($this->options['anonymous_text']);
      }

      // If we are to display the name as a link and the user is not anonymous
      // then pass it through the theme function.
      $output = $account->name;
      if (!empty($this->options['link_to_sender']) && $account->uid != 0) {
        $output = theme('username', $account);
      }
    }
    else {
      $group = node_load($gid);

      // If we are to display the name as a link then link the title to the
      // group node.
      $output = $group->title;
      if (!empty($this->options['link_to_sender'])) {
        $output = l($group->title, 'node/' . $group->nid);
      }
    }

    return $output;
  }
}
