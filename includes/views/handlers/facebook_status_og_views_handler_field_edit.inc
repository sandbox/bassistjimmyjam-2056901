<?php
/**
 * @file
 * Contains facebook_status_og_views_handler_field_edit.
 */

/**
 * Displays an edit link for a status if the user it authorized to edit it.
 */
class facebook_status_og_views_handler_field_edit extends facebook_status_views_handler_field_edit {
  /**
   * Construct a new field handler.
   */
  function construct() {
    parent::construct();

    $this->additional_fields['gid'] = 'gid';
  }

  /**
   * Render the field.
   *
   * This is largely the same as the parent, except for the addition of gid to
   * the status object.
   */
  function render($values) {
    $status = new stdClass();
    $status->sender = $this->get_value($values, 'sender');
    $status->recipient = $this->get_value($values, 'recipient');
    $status->type = $this->get_value($values, 'type');
    $status->gid = $this->get_value($values, 'gid');
    $status->sid = $this->get_value($values);

    // If the user has access to edit the status then display the edit link.
    if (facebook_status_user_access('edit', $status)) {
      if (module_exists('modalframe')) {
        modalframe_parent_js();
      }

      drupal_add_css(drupal_get_path('module', 'facebook_status') . '/resources/facebook_status.css');
      $q = $_GET['q'];
      if ($q == 'facebook_status/js') {
        $q = 'user';
      }

      return '<span class="facebook-status-edit">' . l(t('Edit'), 'statuses/' . $status->sid . '/edit', array('query' => array('destination' => $q))) . '</span>';
    }
  }
}
