<?php
/**
 * @file
 * Default views for the Facebook-style Statuses OG module.
 */

/**
 * Implements hook_views_default_views_alter().
 *
 * Alter views provided by the facbook_status module to handle the new OG
 * integration features.
 */
function facebook_status_og_views_default_views_alter(&$data) {
  if (isset($data['fbss_ur_stream'])) {
    _facebook_status_og_fbss_ur_stream_alter($data['fbss_ur_stream']);
  }

  if (isset($data['facebook_status_stream'])) {
    _facebook_status_og_facebook_status_stream_alter($data['facebook_status_stream']);
  }
}

/**
 * Alters the fbss_ur_stream view to handle new OG integration features.
 *
 * @param stdClass $view
 *   The fbss_ur_stream view object to be altered.
 */
function _facebook_status_og_fbss_ur_stream_alter(&$view) {
  $display = &$view->display['default'];

  // Add the group node relationship.
  $display->display_options['relationships']['node']['id'] = 'node';
  $display->display_options['relationships']['node']['table'] = 'facebook_status';
  $display->display_options['relationships']['node']['field'] = 'node';
  $display->display_options['relationships']['node']['required'] = 0;

  // Add a new filter group and move the type filter into that group. This will
  // be used to filter statuses where the type is user profile OR it is from a
  // group that the user is a member of.
  $display->display_options['filter_groups']['groups'][1] = 'OR';
  $display->display_options['filters']['type']['group'] = '1';

  // Add the group membership filter.
  $display->display_options['filters']['uid']['id'] = 'uid';
  $display->display_options['filters']['uid']['table'] = 'og_uid';
  $display->display_options['filters']['uid']['field'] = 'uid';
  $display->display_options['filters']['uid']['relationship'] = 'node';
  $display->display_options['filters']['uid']['value'] = '1';
  $display->display_options['filters']['uid']['group'] = '1';

  // We need to replace the user name field with the sender name field, so slice
  // the array up and add the sender name in the correct location.
  if (!empty($display->display_options['fields'])) {
    $fields = array_slice($display->display_options['fields'], 0, 3, TRUE);
    $fields['sender_name'] = _facebook_status_og_views_sender_name_field();
    $fields += array_slice($display->display_options['fields'], 4, NULL, TRUE);
  }

  // Update the text for the custom text arrea to use the sender name rather
  // than the user name.
  $fields['nothing']['alter']['text'] = str_replace('[name]', '[sender_name]', $fields['nothing']['alter']['text']);

  // Update the fields on the display.
  $display->display_options['fields'] = $fields;
}

/**
 * Alters the facebook_status_stream view to handle new OG integration features.
 *
 * @param stdClass $view
 *   The facebook_status_stream view object to be altered.
 */
function _facebook_status_og_facebook_status_stream_alter(&$view) {
  $display = &$view->display['default'];

  // We need to replace the user name field with the sender name field, so slice
  // the array up and add the sender name in the correct location.
  if (!empty($display->display_options['fields'])) {
    $fields = array_slice($display->display_options['fields'], 0, 2, TRUE);
    $fields['sender_name'] = _facebook_status_og_views_sender_name_field();
    $fields += array_slice($display->display_options['fields'], 3, NULL, TRUE);
  }

  // Update the text for the custom text arrea to use the sender name rather
  // than the user name.
  $fields['nothing']['alter']['text'] = str_replace('[name]', '[sender_name]', $fields['nothing']['alter']['text']);

  // Update the fields on the display.
  $display->display_options['fields'] = $fields;
}

/**
 * Builds the sender name field as it would be used for a view.
 *
 * @return array
 *   The sender name field formatter for a view.
 */
function _facebook_status_og_views_sender_name_field() {
  return array(
    'id' => 'sender_name',
    'table' => 'facebook_status',
    'field' => 'sender_name',
    'label' => '',
    'exclude' => TRUE,
    'alter' => array(
      'alter_text' => 1,
      'text' => '<span class="facebook-status-sender">[sender_name]</span>',
      'make_link' => 0,
      'absolute' => 0,
      'external' => 0,
      'replace_spaces' => 0,
      'trim_whitespace' => 0,
      'nl2br' => 0,
      'word_boundry' => 1,
      'ellipsis' => 1,
      'strip_tags' => 0,
      'trim' => 0,
      'html' => 0,
    ),
    'element_label_colon' => 1,
    'element_default_classes' => 1,
    'hide_empty' => 0,
    'empty_zero' => 0,
    'hide_alter_empty' => 1,
    'link_to_sender' => 1,
    'overwrite_anonymous' => 0,
  );
}
