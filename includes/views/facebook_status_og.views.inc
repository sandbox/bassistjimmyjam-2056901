<?php
/**
 * @file
 * Views hook implementations for the Facebook-style Statuses OG module.
 */

/**
 * Implements hook_views_data().
 */
function facebook_status_og_views_data() {
  // Add a field handler for the sender name that takes into account statuses
  // that were posted as a group.
  $data['facebook_status']['sender_name'] = array(
    'real field' => 'sender',
    'title' => t('Sender name'),
    'help' => t('The name of the statuses sender.'),
    'field' => array(
      'handler' => 'facebook_status_og_views_handler_field_sender_name',
      'click sortable' => FALSE,
    ),
  );

  // Make it possible to add a relationship to the node table for statuses
  // posted as a group.
  $data['facebook_status']['node'] = array(
    'group' => t('Facebook-style Statuses'),
    'relationship' => array(
      'title' => t('Group node'),
      'label' => t('Group node'),
      'help' => t('Add a relationship to the group node for statuses posted as a group.'),
      'relationship table' => 'facebook_status',
      'relationship field' => 'gid',
      'base' => 'node',
      'field' => 'nid',
      'type' => 'LEFT',
      'handler' => 'views_handler_relationship',
    ),
  );

  return $data;
}

/**
 * Implements hook_views_data_alter().
 *
 * Overrides some of the handlers provided by the facebook_status module to
 * handle statuses that were posted as a group.
 */
function facebook_status_og_views_data_alter(&$data) {
  $data['facebook_status']['sender_pic']['field']['handler'] = 'facebook_status_og_views_handler_field_sender_pic';
  $data['facebook_status']['edit']['field']['handler'] = 'facebook_status_og_views_handler_field_edit';
}

/**
 * Implements hook_views_handlers().
 */
function facebook_status_og_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'facebook_status_og') . '/includes/views/handlers',
    ),
    'handlers' => array(
      'facebook_status_og_views_handler_field_sender_pic' => array(
        'parent' => 'facebook_status_views_handler_field_sender_pic',
      ),
      'facebook_status_og_views_handler_field_sender_name' => array(
        'parent' => 'views_handler_field',
      ),
      'facebook_status_og_views_handler_field_edit' => array(
        'parent' => 'facebook_status_views_handler_field_edit',
      ),
    ),
  );
}
