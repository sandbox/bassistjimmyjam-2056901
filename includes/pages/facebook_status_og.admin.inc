<?php
/**
 * @file
 * Callbacks for administration pages used by the Facebook-style Statuses OG
 * module.
 */

/**
 * Form callback for the settings page.
 */
function facebook_status_og_config($form_state) {
  $form['image_fields'] = array(
    '#type' => 'fieldset',
    '#title' => t('Image fields'),
    '#description' => t('The fields used to load the image for group statuses for each group content type.'),
  );

  // Iterate over each of the group content types, adding a field for each to
  // allow the user to select which file field will be used for each group type.
  foreach (og_get_types('group') as $group_type) {
    $content_type = content_types($group_type);

    $element_name = 'facebook_status_og_image_field_' . $group_type;
    $form['image_fields'][$element_name] = array(
      '#type' => 'select',
      '#title' => $content_type['name'],
      '#default_value' => (isset($form_state['post'][$element_name])
        ? $form_state['post'][$element_name]
        : variable_get($element_name, '')),
      '#options' => _facebook_status_og_image_field_options($group_type),
    );
  }

  $form['default_image'] = array(
    '#type' => 'fieldset',
  );
  $form['default_image']['facebook_status_og_image_default'] = array(
    '#type' => 'textfield',
    '#title' => t('Default image'),
    '#description' => t('The default image to use for groups that have no image set.'),
    '#default_value' => (isset($form_state['post']['facebook_status_og_image_default'])
      ? $form_state['post']['facebook_status_og_image_default']
      : variable_get('facebook_status_og_image_default', '')),
  );

  // If we have a default value and it has been set then add a preview of the
  // image to the form.
  $default_image = $form['default_image']['facebook_status_og_image_default']['#default_value'];
  if (!empty($default_image) && file_exists(DRUPAL_ROOT . '/' . $default_image)) {
    $alt = t('Default group image preview.');
    $form['default_image']['image_preview'] = array(
      '#type' => 'markup',
      '#value' => '<div>' . theme('image', $default_image, $alt, $alt, '', FALSE) . '<div>',
    );
    $form['default_image']['image_preview_desc'] = array(
      '#type' => 'markup',
      '#value' => '<div>' . t('When displayed in a status feed, your default image will use the imagecache preset selected in the view rather than being full size.') . '<div>',
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#validate' => array('facebook_status_og_config_validate'),
    '#submit' => array('facebook_status_og_config_submit'),
  );

  return $form;
}

/**
 * Retrieves options for a group content type's image field.
 *
 * Determine's which file fields exist on the content type and returns them,
 * sorted alphabetically.
 *
 * @param string $group_type
 *   The group content type to get the options for.
 *
 * @return array
 *   The file fields available for the group content type.
 */
function _facebook_status_og_image_field_options($group_type) {
  // Iterate over all of the fields for the content type and add any file fields
  // to the options.
  $options = array();
  foreach (content_fields(NULL, $group_type) as $field_name => $field) {
    if ($field['type'] == 'filefield' && $field['type_name'] = 'group') {
      $options[$field_name] = $field['widget']['label'];
    }
  }

  // Sort the fields alphabetically.
  asort($options);

  return array_unique($options);
}

/**
 * Validation callback for the settings form.
 */
function facebook_status_og_config_validate($form, &$form_state) {
  // If a default image has been specified but it does not exist, we need to
  // fail validation.
  if (
    !empty($form_state['values']['facebook_status_og_image_default'])
    && !file_exists(DRUPAL_ROOT . '/' . $form_state['values']['facebook_status_og_image_default'])
  ) {
    form_set_error('facebook_status_og_image_default', t('The file that you specified does not exist.'));
  }
}

/**
 * Submit callback for the settings form.
 */
function facebook_status_og_config_submit($form, &$form_state) {
  variable_set('facebook_status_og_image_default', $form_state['values']['facebook_status_og_image_default']);

  // Iterate over the group content types, setting the selected file field for
  // each.
  foreach (element_children($form['image_fields']) as $element_name) {
    variable_set($element_name, $form_state['values'][$element_name]);
  }
}
