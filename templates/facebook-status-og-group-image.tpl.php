<?php
/**
 * @file
 * Default theme implementation to present an image for a group.
 *
 * Available variables:
 * - $picture: Image set for the group or the site's default. Will be linked to
 *   the group node.
 * - $group: Group node object. Potentially unsafe. Be sure to check_plain()
 *   before use.
 */
?>
<div class="picture">
  <?php print $picture; ?>
</div>
